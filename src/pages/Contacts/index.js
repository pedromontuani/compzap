import React, { useState, useEffect } from 'react';
import { View, FlatList, TextInput } from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Button,
  Icon,
  Title
} from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';

import ChatItem from '../../components/ChatItem';

import styles from './styles';
import colors from '../../theme/colors';
import { getUsers } from '../../services/firestoreService';

const Contacts = props => {
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    getUsers().then(users => {
      setContacts(users.docs);
    });
  }, []);

  const normalizeText = str => {
    str = str.replace(/[ÀÁÂÃÄÅ]/, 'A');
    str = str.replace(/[àáâãäå]/, 'a');
    str = str.replace(/[ÈÉÊË]/, 'E');
    str = str.replace(/[èéêë]/, 'e');
    str = str.replace(/[ÍÌÏ]/, 'I');
    str = str.replace(/[íìï]/, 'i');
    str = str.replace(/[ÔÕÖ]/, 'O');
    str = str.replace(/[õôö]/, 'ô');
    str = str.replace(/[ÚÜ]/, 'U');
    str = str.replace(/[úü]/, 'u');
    str = str.replace(/[Ç]/, 'C');
    str = str.replace(/[ç]/, 'c');
    str.replace(/[^a-z0-9]/gi, '');

    return str.toLowerCase();
  };

  const filterByText = contactName => {
    if (searchTerm.length > 0) {
      const name = normalizeText(contactName);
      const search = normalizeText(searchTerm);
      return name.indexOf(search) > -1;
    } else {
      return true;
    }
  };

  return (
    <Container>
      <Header androidStatusBarColor={colors.primaryDark} style={styles.header}>
        <Left>
          <TouchableNativeFeedback>
            <Button transparent onPress={() => props.navigation.goBack()}>
              <Icon name={'arrow-back'} />
            </Button>
          </TouchableNativeFeedback>
        </Left>
        <Body>
          <Title>Contatos</Title>
        </Body>
        <Right />
      </Header>

      <View style={styles.content}>
        <View style={styles.searchboxOuter}>
          <View style={styles.searchboxInner}>
            <View style={styles.searchIconHolder}>
              <Icon
                name={'search'}
                type={'FontAwesome'}
                style={styles.searchIcon}
              />
            </View>
            <TextInput
              placeholder={'Pesquisar...'}
              style={styles.searchInput}
            />
          </View>
        </View>
        <FlatList
          contentContainerStyle={styles.contacsList}
          data={contacts}
          keyExtractor={item => item.id}
          renderItem={({ item }) => (
            <ChatItem avatar={item.data().avatar} name={item.data().name} />
          )}
        />
      </View>
    </Container>
  );
};

export default Contacts;
