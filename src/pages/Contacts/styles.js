import { StyleSheet } from 'react-native';
import colors from '../../theme/colors';

export default StyleSheet.create({
    header: {
        backgroundColor: colors.primary
    },
    content: {
        flex: 1
    },
    searchboxOuter: {
        height: 80,
        padding: 15,
        elevation: 3,
        backgroundColor: "#FFFFFF"
    },
    searchboxInner: {
        flex: 1,
        borderWidth: 1,
        borderColor: '#C0C0C0',
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 40,
        flexDirection: 'row'
    },
    searchIconHolder: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    searchIcon: {
        fontSize: 16,
        color: '#909090',
        marginTop: -3
    },
    searchInput: {
        width: '95%',
        paddingLeft: 10,
        marginBottom: -1
    },
    contactsList: {
        paddingBottom: 10
    }
});