import React, { useEffect } from 'react';
import { View, ActivityIndicator } from 'react-native';

const Startup = props => {
  let unsubscribe = undefined;

  useEffect(() => {}, []);

  useEffect(() => {
    return () => {};
  }, []);

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
      }}
    >
      <ActivityIndicator />
    </View>
  );
};

export default Startup;
