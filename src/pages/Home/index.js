import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import {
  Container,
  Header,
  Body,
  Title,
  Right,
  Button,
  Icon,
  Fab
} from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import moment from 'moment';

import styles from './styles';
import colors from '../../theme/colors';

const Home = props => {
  const extractTimestamp = timestamp => {
    const fullTime = timestamp.toDate();

    if (new Date().toDateString() == fullTime.toDateString()) {
      return moment(fullTime).format('HH:mm');
    } else {
      return moment(fullTime).format('DD/MM/YY HH:mm');
    }
  };

  return (
    <Container>
      <Header
        noLeft
        androidStatusBarColor={colors.primaryDark}
        style={styles.header}
      >
        <Body>
          <Title>Chats</Title>
        </Body>
        <Right>
          <TouchableNativeFeedback>
            <Button transparent onPress={() => {}}>
              <Icon name={'sign-out'} type={'FontAwesome'} />
            </Button>
          </TouchableNativeFeedback>
        </Right>
      </Header>

      <View style={styles.content}>
        {
          // Lista de conversas vem aqui
        }
      </View>

      <Fab
        style={styles.fab}
        onPress={() => props.navigation.navigate('Contacts')}
      >
        <Icon name="plus" type="FontAwesome" />
      </Fab>
    </Container>
  );
};

export default Home;
