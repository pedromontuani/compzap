import React, { useState } from 'react';
import {
  Text,
  View,
  TextInput,
  ImageBackground,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import { Spinner } from 'native-base';

import styles from './styles';
import colors from '../../theme/colors';
import RoundButton from '../../components/RoundButton';

import { emailAndPasswordSignUp } from '../../services/authService';

const SignUp = props => {
  const [avatar, setAvatar] = useState(undefined);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const imagePickerOptions = {
    mediaType: 'photo',
    maxWidth: 500,
    maxHeight: 500,
    rotation: 0,
    noData: true
  };

  const selectAvatar = () => {
    ImagePicker.launchImageLibrary(imagePickerOptions, res => {
      setAvatar(res);
    });
  };

  const SignUp = () => {
    emailAndPasswordSignUp(name, email, password, avatar)
      .then(() => {
        props.navigation.navigate('Home');
      })
      .catch(error => {
        console.log('Erro ao criar usuário', error);
      });
  };

  return (
    <ImageBackground
      style={styles.viewport}
      source={require('../../theme/assets/login-background/background.png')}
      resizeMode={'cover'}
    >
      <KeyboardAvoidingView
        behavior={'position'}
        contentContainerStyle={styles.container}
        style={styles.kbAvoindingView}
      >
        <TouchableOpacity onPress={selectAvatar} style={styles.avatarHolder}>
          <Image
            style={styles.avatar}
            source={
              avatar && avatar.uri
                ? { uri: avatar.uri }
                : require('../../theme/assets/no-photo.jpg')
            }
            resizeMode={'cover'}
          />
          <Icon name="plus-circle" style={styles.avatarSelection} />
        </TouchableOpacity>

        <View style={styles.inputsHolder}>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Nome'}
            autoCompleteType={'name'}
            textContentType={'name'}
            autoCapitalize={'words'}
            onChangeText={text => setName(text)}
          ></TextInput>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Email'}
            autoCompleteType={'email'}
            keyboardType={'email-address'}
            textContentType={'emailAddress'}
            autoCapitalize={'none'}
            onChangeText={text => setEmail(text)}
          ></TextInput>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Senha'}
            textContentType={'password'}
            secureTextEntry={true}
            onChangeText={text => setPassword(text)}
          ></TextInput>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Repetir senha'}
            textContentType={'password'}
            secureTextEntry={true}
            onChangeText={text => setVerifyPassword(text)}
          ></TextInput>
        </View>
      </KeyboardAvoidingView>
      <View style={styles.buttonsHolder}>
        <RoundButton
          backgroundColor={'rgba(255, 255, 255, 1)'}
          borderColor={'transparent'}
          onPress={() => {
            SignUp();
          }}
        >
          {loading ? (
            <Spinner color={colors.primary} />
          ) : (
            <Text style={styles.textFullButton}>CADASTRE-SE</Text>
          )}
        </RoundButton>
        <RoundButton
          backgroundColor={'transparent'}
          borderColor={'transparent'}
          onPress={() => {
            props.navigation.goBack();
          }}
        >
          <Text style={styles.textOutlineButton}>
            Já possui uma conta?
            <Text style={{ fontWeight: 'bold' }}> Faça o login!</Text>
          </Text>
        </RoundButton>
      </View>
    </ImageBackground>
  );
};

export default SignUp;
