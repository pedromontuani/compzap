import React, { useState } from 'react';
import { Text, View, TextInput, ImageBackground, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Spinner } from 'native-base';

import colors from '../../theme/colors';
import styles from './styles';
import RoundButton from '../../components/RoundButton';

import { signIn } from '../../services/authService';

const Login = props => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const SignIn = () => {
    signIn(email, password)
      .then(user => {
        console.log('Dados usuário', user);
        props.navigation.navigate('Home');
      })
      .catch(error => {
        alert('Ocorreu um erro. Tente novamente.');
        console.log('Erro de login', error);
      });
  };

  return (
    <ImageBackground
      style={styles.viewport}
      source={require('../../theme/assets/login-background/background.png')}
      resizeMode={'cover'}
    >
      <View style={styles.container}>
        <View style={styles.logoHolder}>
          <Image
            style={styles.firebaseLogo}
            source={require('../../theme/assets/firebase-logo/firebase.png')}
            resizeMode={'contain'}
          />
        </View>

        <View style={styles.inputsHolder}>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Email'}
            autoCompleteType={'email'}
            keyboardType={'email-address'}
            textContentType={'emailAddress'}
            autoCapitalize={'none'}
            onChangeText={text => setEmail(text)}
          ></TextInput>

          <TextInput
            style={styles.textInput}
            underlineColorAndroid={'#FFFFFF'}
            placeholderTextColor={'#FFFFFF'}
            placeholder={'Senha'}
            textContentType={'password'}
            secureTextEntry={true}
            onChangeText={text => setPassword(text)}
          ></TextInput>
        </View>

        <View style={styles.buttonsHolder}>
          <RoundButton
            backgroundColor={'#FFFFFF'}
            borderColor={'transparent'}
            onPress={() => {
              SignIn();
            }}
          >
            <Text style={styles.textFullButton}>LOGIN</Text>
          </RoundButton>
          <RoundButton
            backgroundColor={'transparent'}
            borderColor={'#FFFFFF'}
            onPress={() => {}}
          >
            <Icon
              name={'google-plus'}
              style={[styles.textOutlineButton, styles.socialIcon]}
            />
          </RoundButton>
          <RoundButton
            backgroundColor={'transparent'}
            borderColor={'transparent'}
            onPress={() => {
              props.navigation.navigate('SignUp');
            }}
          >
            <Text style={styles.textOutlineButton}>
              Não possui uma conta?
              <Text style={{ fontWeight: 'bold' }}> Cadastre-se!</Text>
            </Text>
          </RoundButton>
        </View>
      </View>
    </ImageBackground>
  );
};

export default Login;
