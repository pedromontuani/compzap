import React, { useState, useEffect } from 'react';
import { View, FlatList, TextInput, ImageBackground } from 'react-native';
import {
  Container,
  Header,
  Footer,
  Left,
  Right,
  Body,
  Button,
  Icon,
  Title,
  Thumbnail
} from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import moment from 'moment';

import styles from './styles';
import colors from '../../theme/colors';

const Chat = props => {
  const extractTimestamp = timestamp => {
    const fullTime = timestamp.toDate();

    if (new Date().toDateString() == fullTime.toDateString()) {
      return moment(fullTime).format('HH:mm');
    } else {
      return moment(fullTime).format('DD/MM/YY HH:mm');
    }
  };

  return (
    <ImageBackground
      source={require('../../theme/assets/chat-background/background.png')}
      style={styles.imageBackground}
    >
      <Container style={styles.container}>
        <Header
          androidStatusBarColor={colors.primaryDark}
          style={styles.header}
        >
          <Left>
            <TouchableNativeFeedback>
              <Button transparent onPress={() => props.navigation.goBack()}>
                <Icon name={'arrow-back'} />
              </Button>
            </TouchableNativeFeedback>
          </Left>
          <Body style={styles.contactInfo}>
            <Thumbnail
              style={styles.contactAvatar}
              source={require('../../theme/assets/no-photo.jpg')}
            />
            <Title style={styles.contactName}>Contato</Title>
          </Body>
          <Right />
        </Header>

        <View style={styles.content}>
          {
            // Lista de mensagens aqui
          }
        </View>

        <Footer style={styles.footer}>
          <Body style={styles.inputHolder}>
            <View style={styles.inputOuter}>
              <TextInput placeholder={'Digite aqui...'} value={message} />
            </View>
          </Body>
          <Right style={styles.buttonHolder}>
            <Button style={styles.sendButton} onPress={() => {}}>
              <Icon
                name={'send'}
                type={'MaterialIcons'}
                style={styles.sendIcon}
              />
            </Button>
          </Right>
        </Footer>
      </Container>
    </ImageBackground>
  );
};

export default Chat;
