import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../theme/colors';

const deviceHeigth = Dimensions.get('window').height - 20;

export default StyleSheet.create({
  imageBackground: {
    flex: 1
  },
  container: {
    backgroundColor: 'transparent'
  },
  header: {
    backgroundColor: colors.primary
  },
  contactInfo: {
    flexDirection: 'row',
    marginLeft: -25
  },
  contactAvatar: {
    height: 40,
    width: 40
  },
  contactName: {
    alignSelf: 'center',
    paddingLeft: 15
  },
  content: {
    flex: 1,
    paddingBottom: 5
  },
  footer: {
    backgroundColor: 'transparent',
    elevation: 0,
    paddingBottom: 10
  },
  inputHolder: {
    paddingLeft: 10
  },
  inputOuter: {
    backgroundColor: '#FFF',
    width: '100%',
    borderRadius: 25,
    paddingHorizontal: 10,
    elevation: 1
  },
  buttonHolder: {
    flex: 0.18,
    paddingRight: 5
  },
  sendButton: {
    height: 50,
    width: 50,
    borderRadius: 25,
    backgroundColor: colors.secondary,
    alignSelf: 'flex-end'
  },
  sendIcon: {
    marginLeft: 14,
    marginRight: 10
  }
});
