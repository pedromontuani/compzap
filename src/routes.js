import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Startup from './pages/Startup';
import Home from './pages/Home';
import Login from './pages/Login';
import SignUp from './pages/SignUp';
import Contacts from './pages/Contacts';
import Chat from './pages/Chat';

const AuthRoutes = createStackNavigator(
  {
    Login: {
      screen: Login
    },
    SignUp: {
      screen: SignUp
    }
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

const UserRoutes = createStackNavigator(
  {
    Home: {
      screen: Home
    },
    Contacts: {
      screen: Contacts
    },
    Chat: {
      screen: Chat
    }
  },
  {
    defaultNavigationOptions: {
      header: null
    }
  }
);

/*const Routes = createAppContainer(  // Switch navigator: rotas para separar parte acessível
  createSwitchNavigator(               // a usuários logados, e parte para usuários logados
    {
      Auth: {
        screen: AuthRoutes
      },
      User: {
        screen: UserRoutes
      }
    },
    {
      initialRouteName: 'Auth'
    }
  )
);*/

const Routes = createAppContainer(
  createStackNavigator(
    {
      Login: {
        screen: Login
      },
      SignUp: {
        screen: SignUp
      },
      Home: {
        screen: Home
      },
      Contacts: {
        screen: Contacts
      },
      Chat: {
        screen: Chat
      }
    },
    {
      defaultNavigationOptions: {
        header: null
      }
    }
  )
);

export default Routes;
