export default {
  borderRadius: 7,
  pagePadding: 30,
  itemPadding: 20
};
