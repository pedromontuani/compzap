import firebase from 'react-native-firebase';

const getUsers = () => {
  return firebase
    .firestore()
    .collection('users')
    .get();
};

export { getUsers };
