import firebase from 'react-native-firebase';
import { GoogleSignin } from '@react-native-community/google-signin';
import { saveFile } from './storageService';

const signIn = (email, password) => {
  return firebase.auth().signInWithEmailAndPassword(email, password);
};

const emailAndPasswordSignUp = (name, email, password, photo) => {
  return firebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(data => {
      console.log('Usuário criado', data);
      const userObjReference = firebase
        .firestore()
        .collection('users')
        .doc(data.user.uid);

      return userObjReference
        .set({
          name: name
        })
        .then(() => {
          console.log('Dados do usuário salvos.');
          if (photo) {
            return firebase
              .storage()
              .ref(`users/${data.user.uid}/avatar.jpg`)
              .putFile(photo.uri, { contentType: photo.type })
              .then(fileObj => {
                console.log('Arquivo salvo.');
                userObjReference
                  .update({
                    avatar: fileObj.downloadURL
                  })
                  .then(() => {
                    console.log('Foto de perfil atualizada');
                  });
              });
          }
        });
    });
};

export { signIn, emailAndPasswordSignUp };
