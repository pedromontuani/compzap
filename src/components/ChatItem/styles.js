import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  itemHolder: {
    backgroundColor: '#FFFFFF'
  },
  listItem: {
   
  },
  avatar: {
    marginTop: -4
  },
  contactName: {
      fontWeight: 'bold',
      fontSize: 16,
      color: '#252525'
  },
  subtitle: {
    color: '#505050',
    paddingVertical: 5
  },
  textSpacing: {
    paddingVertical: 12
  },
  iconRight: {
    alignSelf: 'center'
  }
});
