import React from 'react';
import { View } from 'react-native';

import styles from './styles';
import { TouchableOpacity } from 'react-native';

const RoundButton = ({
  onPress,
  backgroundColor,
  borderColor,
  disabled,
  children
}) => {
  return (
    <View style={styles.buttonHolder}>
      <TouchableOpacity disabled={disabled} onPress={onPress}>
        <View
          style={[
            styles.buttonOuter,
            { backgroundColor: backgroundColor, borderColor: borderColor }
          ]}
        >
          <View style={styles.buttonInner}>
            <View
              style={{
                alignSelf: 'stretch',
                flexDirection: 'row',
                justifyContent: 'center'
              }}
            >
              {children}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default RoundButton;
