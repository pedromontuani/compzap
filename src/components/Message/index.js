import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

const Message = ({ userUid, senderUid, message, timestamp }) => {
  return (
    <View style={styles.messageOuter}>
      <View
        style={[
          styles.messageInner,
          userUid == senderUid ? styles.userStyle : styles.contactStyle
        ]}
      >
        <Text>{message}</Text>
        <Text style={styles.timestamp}>{timestamp}</Text>
      </View>
    </View>
  );
};

export default Message;
