import { StyleSheet } from 'react-native';
import colors from '../../theme/colors';

export default StyleSheet.create({
  messageOuter: {
    paddingHorizontal: 10,
    paddingVertical: 5
  },
  messageInner: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
    elevation: 2,
    maxWidth: '80%',
    minWidth: '20%'
  },
  timestamp: {
    alignSelf: 'flex-end',
    paddingTop: 2,
    color: '#808080',
    fontSize: 12
  },
  userStyle: {
    alignSelf: 'flex-end',
    backgroundColor: '#DBF9C5'
  },
  contactStyle: {
    alignSelf: 'flex-start',
    backgroundColor: '#FFF'
  }
});
